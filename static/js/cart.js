var add = "add"
var remove = "remove"

function addcart(a,b){
    var productId = a
    var action = b
    console.log('productId:', productId, 'action:', action)
    console.log('USER:', user)
    if(user == 'AnonymousUser'){
        console.log('not logged in')
    } else {
        updateUserOrder(productId, action)
    }
}

function updateUserOrder(productId, action){
    var url = '/update_item/'

    fetch(url, {
        method:'POST',
        headers:{
            'Content-Type':'application/json',
            'X-CSRFToken':csrftoken,
        },
        body: JSON.stringify({'productId': productId, 'action': action})
    })

    .then((response) =>{
        return response.json()
    })
    .then((data) =>{
        console.log('data:', data)

        location.reload()
    })
}